//#include <SPI.h>
//#include <Wire.h>
//#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <cstdlib>

void setup() {

  std::srand(time(0));
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Hi");
  delay(1500);
}

void loop() {
  // gives values between 20 and 40 -> inspired by https://en.cppreference.com/w/cpp/numeric/random/rand
  int sensor_value = std::rand()/((RAND_MAX + 1u)/20) + 20;
  Serial.println(sensor_value);
  delay(1000);
}
